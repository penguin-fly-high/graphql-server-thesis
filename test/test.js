import assert from "assert";
import Product from "../src/models/Product";

describe("test product schema", function() {
  it("should save product", function() {
    const product = new Product({
      name: "Orange",
      location: "LamDong"
    });
    product.save();

    const searchedProduct = Product.find()
      .or([{ name: product.name }])
      .then(res => {
        assert.equal(searchedProduct.name, product.name);
      });
  });
});
