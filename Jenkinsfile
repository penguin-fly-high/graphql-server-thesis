node("ubuntu-sonar-scanner") {
  bitbucketStatusNotify(buildState: 'INPROGRESS')
  def proccess = "BUILD"
  git credentialsId: '09aa345f-d634-4c61-9cb9-33ae2a586255', url: 'git@bitbucket.org:penguin-fly-high/graphql-server-thesis.git'
  try {
    notifyBuildStatus("STARTED", proccess)
    stage("Install dependencies") {
      nodejs('nodejs') {
        sh "npm install"
      }
    }

    stage("Inspect code") {
      nodejs('nodejs') {
      sh "npm run prev-start"
      }
    }

    stage("Scan code and push to SonarQube") {
      def scannerHome = tool name: 'sonar-scanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
      withSonarQubeEnv {
        sh "${scannerHome}/bin/sonar-scanner -Dsonar.host.url=https://sonarcloud.io -Dsonar.sources=$WORKSPACE/src -Dsonar.projectKey=Trantuanphuong031294 -Dsonar.organization=penguin-fly-high-bitbucket"
      }
    }

    stage('Quality Gate') {
        timeout(time: 1, unit: 'HOURS') {
          def qg = waitForQualityGate()
          if (qg.status != 'OK' && qg.status != 'WARN') {
            err "Pipeline aborted due to quality gate "
          }
        }
    }
    notifyBuildStatus('SUCCESSFUL', proccess)
  } catch (e) {
    notifyBuildStatus('FAILURE', proccess)
    bitbucketStatusNotify(buildState: 'FAILED')
    throw e
  } 
}

node("ubuntu-nodejs-host") {
  def proc = 'DEPLOY'
  try {
    notifyBuildStatus("STARTED", proc)
    git credentialsId: '90794b96-4bb0-4310-8a60-93055403eac0', url: 'git@bitbucket.org:penguin-fly-high/graphql-server-thesis.git'

    stage("Install dependencies") {
      nodejs('nodejs') {
        sh "npm install"
      }
    }

    stage("Build project") {
      nodejs('nodejs') {
        sh "npm run build"
      }
    }

    stage("Clean up docker stuff") {
      sh 'docker ps -q --filter ancestor="graphql-nodejs-server" | xargs -r docker stop'
      sh 'docker container prune -f'
      sh 'docker image prune -f'
    }

    stage("Build docker image from Dockerfile") {
      sh "docker build -t graphql-nodejs-server ."
    }

    stage("Run docker container") {
      sh "docker run -d -v /var/lib/ubuntu/workspace/images-upload:/app/uploads -p 4000:4000 graphql-nodejs-server"
    } 
    notifyBuildStatus('SUCCESSFUL', proc)   
    bitbucketStatusNotify(buildState: 'SUCCESSFUL') 
  } catch (e) {
    notifyBuildStatus('FAILURE', proc)
    bitbucketStatusNotify(buildState: 'FAILED')
    throw e
  } 
}

def notifyBuildStatus(String buildStatus = 'STARTED', String proc) {
  buildStatus = buildStatus ?: 'SUCCESSFUL'
  // Default values
   def colorName = 'RED'
   def colorCode = '#FF0000'
   def subject = "${proc} process ${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
   def summary = "${subject} (${env.BUILD_URL})"
   def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
     <p>Check console output at "<a href="${env.BUILD_URL}">${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p>"""

    // Override default values based on build status
   if (buildStatus == 'STARTED') {
     color = 'YELLOW'
     colorCode = '#FFFF00'
   } else if (buildStatus == 'SUCCESSFUL') {
     color = 'GREEN'
     colorCode = '#00FF00'
   } else {
     color = 'RED'
     colorCode = '#FF0000'
   }  

   emailext (
     subject: subject,
     body: details,
     to: '$DEFAULT_RECIPIENTS'
   ) 
}