import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const UserSchema = new Schema({
  username: String,
  password: String,
  age: Number,
  address: {
    street: String,
    district: String,
    ward: String,
    city: String
  },
  emailAddress: String,
  createdDate: String,
  updatedDate: String,
  role: { type: String, default: 'user' },
  purchasedItems: [
    {
      productId: String,
      purchasedDate: Date,
      quantity: Number
    }
  ],
});

export default mongoose.model('User', UserSchema);