import mongoose from "mongoose";

const Schema = mongoose.Schema;
const ProductSchema = new Schema({
  name: String,
  category: String,
  classification: String,
  location: String,
  farmerId: String,
  grownPeriod: Date,
  quantity: Number,
  recentPrice: Number,
  imageUrl: String,
  userComments: [{
    userId: String,
    username: String,
    content: String,
    createdDate: Date
  }],
  priceOverPeriodOfTime: [new Schema({
    price: Number,
    updatedDate: Date
  }), {_id: false}],
  buyers: [
    new Schema({
      buyerId: String,
      purchasedDate: Date
    })
  ],
  createdDate: Date,
  updatedDate: Date
});

export default mongoose.model("Product", ProductSchema);