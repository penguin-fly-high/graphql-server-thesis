import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const BuyerSchema = new Schema({
  buyerName: String,
  age: Number,
  address: String,
  username: String,
  password: String,
  createdDate: String,
  updatedDate: String,
  purchasedItems: [
    {
      productId: String,
      purchasedDate: Date,
      quantity: Number
    }
  ]
});

export default mongoose.model('Buyer', BuyerSchema);