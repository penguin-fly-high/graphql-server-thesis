import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const FarmerSchema = new Schema({
  name: String,
  address: String,
  age: String,
  imageUrl: String
});

export default mongoose.model('Farmer', FarmerSchema);