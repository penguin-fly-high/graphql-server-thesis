import * as Promise from 'bluebird';
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import graphHTTP from "express-graphql";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import multer from 'multer';
import Product from "./models/Product";
import schema from "./schema/schema";
import { MONGO_URI } from "./utils/constants";
import { ResultMaxCount } from "./utils/ResultMaxCount";
import fs, { mkdirSync } from 'fs';
import path from 'path';

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    // const dest = '../images-upload';
    const dest = 'uploads/';
    const dirName = path.resolve(dest);
    if (!fs.existsSync(dirName)) {
      mkdirSync(dirName, {recursive: true});
      console.log('Ax');
    }
    callback(null, dirName);
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "-" + file.originalname);
  }
});

function fileFilter(req, file, cb) {
  if (file.mimetype.indexOf('image') == -1) {
    cb(new Error('File extension not supported'), false);
  } else {
    cb(null, true);
  }
}

const upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

function startServer() {
  const SECRET_KEY = "jfsdourw0729056023--10";

  mongoose.Promise = Promise;
  mongoose
    .connect(MONGO_URI, {
      replicaSet: 'rs'
    })
    .then(() => console.log("DB connected successfully"))
    .catch(err => console.log(err));

  // Allow cross-origin requests

  const app = express();
  app.use(cors());
  // app.use(function(req, res, next) {
  //   res.header("Access-Control-Allow-Origin", "*");
  //   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  //   next();
  // });

  app.get("/", (req, res) => {
    res.send("Hello Friends! This is the demo of CI/CD with Jenkins");
  });

  app.listen(4000, () => {
    console.log("Server is running on port 4000");
  });

  const verifyUser = async (req, res, next) => {
    const token = req.headers.authorization;
    req.user = {
      userId: '',
      username: ''
    };

    try {
      const name = await jwt.verify(token, SECRET_KEY);
      console.log(name);

      if (name) {
        req.user = name;
      }

    } catch (ex) {
      // console.log(ex);

    }

    next();
  };

  app.use('/uploads', express.static('uploads', {
    setHeaders: function(res, path) {
      res.set("Access-Control-Allow-Origin", "*");
      res.set("Access-Control-Allow-Headers", "Content-Type,X-Requested-With");
      res.set("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
      res.set("X-Powered-By",' 3.2.1');
      res.type("application/json");
      res.type("jpg");
      res.type("png");
    }
  }));

  app.use('/static', express.static('uploads'));

  // app.use('/static', express.static(__ + '/uploads'));
  // app.use(express.static('uploads'));

  app.post('/upload', verifyUser, upload.single('image'), async (req, res, next) => {
    console.log(req.file);
    const id = req.body.id;
    console.log(id);
    const product = await Product.findById(id);
    console.log(product.name);
    await Product.update({
      _id: mongoose.Types.ObjectId(id)
    }, {
      $set: {
        imageUrl: req.file.path
      }
    });
    // Store image url to db
    console.log(req.file.path);
    res.send('File uploaded successfully');
    next();
  });

  app.use(multer({
    storage
  }).single('file'));

  app.use(verifyUser);

  const extensions = async ({
    document,
    variables,
    operationName,
    result,
    context
  }) => {
    const keyOfResult = Object.keys(result.data)[0];
    const count = await ResultMaxCount[keyOfResult];
    return {
      runTime: Date.now(),
      count: (result.data[keyOfResult] === null) ? 0 : count
    };
  };

  app.use(
    "/graphql",
    // graphqlUploadExpress({
    //   maxFileSize: 10000000,
    //   maxFiles: 10
    // }),
    bodyParser.json(),
    verifyUser,
    graphHTTP((req) => {
      return {
        schema: schema,
        graphiql: true,
        context: {
          SECRET_KEY,
          userId: req.user.userId,
          username: req.user.username,
          startTime: Date.now()
        },
        rootValue: {
          request: req
        },
        extensions
      };

    })
  );
}



startServer();