let errorProductList = [];
export const addToErrProductList = (product) => {
    errorProductList.push(product);
};

export const getErrorProductList = () => {
    return errorProductList;
};

export const resetErrorProductList = () => {
    errorProductList = [];
};

