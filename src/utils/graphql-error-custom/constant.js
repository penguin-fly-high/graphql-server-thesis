import { getErrorProductList } from './errorProducts';

const products = getErrorProductList();

export const errorName = {
  INSUFFICIENTQUANTITY: 'INSUFFICIENTQUANTITY'
};

export const errorType = {
    INSUFFICIENTQUANTITY: {
    message: 'Insufficient product in stock at moment',
    invalidProducts: products
  }
};

export function getError(errorName) {
    return errorType[errorName];
}
