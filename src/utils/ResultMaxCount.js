import Product from "../models/Product";
import Farmer from "../models/Farmer";
import User from "../models/User";

async function getProductMaxCount() {
    return new Promise(async (resolve, reject) => {
        let result = await Product.count({});
        if (result) {
            resolve(result);
        }
    });
}

async function getFarmerMaxCount() {
    return new Promise(async (resolve, reject) => {
        let result = await Farmer.count({});
        if (result) {
            resolve(result);
        }
    });
}

async function getUserMaxCount() {
    return new Promise(async (resolve, reject) => {
        let result = await User.count({});
        if (result) {
            resolve(result);
        }
    });
}

export const ResultMaxCount = {
    filterBy: getProductMaxCount(),
    searchFarmer: getFarmerMaxCount(),
    searchUser: getUserMaxCount()
};


