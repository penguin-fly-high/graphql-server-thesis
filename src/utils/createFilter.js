export function createUserSearchFilter({ searchBy, searchTerm, ageFrom, ageTo }) {
  let query = [];

  if (searchBy === "Email") {
    query.push({
      emailAddress: { $regex: searchTerm ? searchTerm : '', $options: 'i' }
    });
  }

  if (searchBy === "Username") {
    query.push({
      username: { $regex: searchTerm ? searchTerm : '', $options: 'i' }
    });
  }

  if (ageFrom && ageTo) {
    query.push({
      age: {
        $gte: ageFrom,
        $lte: ageTo
      }
    });
  }
  console.log(query);
  return query;
}

export function createFarmerSearchFilter({ searchBy, searchTerm, ageFrom, ageTo }) {
  let query = [];

  if (searchBy === "Name") {
    query.push({
      name: { $regex: searchTerm? searchTerm : '', $options: 'i' }
    });
  }

  if (searchBy === "Address") {
    query.push({
      address: { $regex: searchTerm? searchTerm : '', $options: 'i' }
    });
  }

  if (ageFrom && ageTo) {
    query.push({
      age: {
        $gte: ageFrom,
        $lte: ageTo
      }
    });
  }
  console.log(query);
  return query;
}