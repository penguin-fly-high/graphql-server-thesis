import {GraphQLInputObjectType, GraphQLID, GraphQLInt} from 'graphql';

const PurchasedProductInputType = new GraphQLInputObjectType({
  name: 'PurchasedProductInput',
  fields: () => ({
    productId: {
      type: GraphQLID
    },
    quantity: {
      type: GraphQLInt
    },
    userId: {
      type: GraphQLID
    },
  })
});

export default PurchasedProductInputType;

