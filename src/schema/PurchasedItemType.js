import {
  GraphQLID,
  GraphQLInt,
  GraphQLObjectType
} from "graphql";
import GraphQLDate from "graphql-date";
import User from "../models/User";
import UserType from "./UserType";

const PurchasedItemType = new GraphQLObjectType({
  name: "PurchasedItem",
  fields: () => ({
    productId: {
      type: GraphQLID
    },
    quantity: {
      type: GraphQLInt,
      async resolve(parent, args) {
        const result = await User.aggregate([{
          $group: {
            productId: parent.productId,
            quantity: {
              $sum: "$quantity"
            }
          }
        }]);
        console.log(result);
      }
    },
    purchasedDate: {
      type: GraphQLDate
    },
    buyerId: {
      type: GraphQLID
    },
    user: {
      type: UserType,
      async resolve(parent, args) {
        console.log('Buyer id', parent.buyerId);
        let result = await User.findById(parent.buyerId);
        console.log('User', result);
        return result;
      }
    }
  })
});

export default PurchasedItemType;