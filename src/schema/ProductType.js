import {
  GraphQLID,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLList
} from "graphql";
import Farmer from "../models/Farmer";
import FarmerType from "./FarmerType";
import Product from "../models/Product";
import PriceOverTimeType from "./PriceOverTimeType";
import mongoose from 'mongoose';
import UserType from "./UserType";
import PurchasedItemType from "./PurchasedItemType";
import User from "../models/User";

const ProductType = new GraphQLObjectType({
  name: "Product",
  fields: () => ({
    id: {
      type: GraphQLID
    },
    name: {
      type: GraphQLString
    },
    category: {
      type: GraphQLString
    },
    classification: {
      type: GraphQLString
    },
    location: {
      type: GraphQLString
    },
    farmerId: {
      type: GraphQLID
    },
    quantity: {
      type: GraphQLInt
    },
    recentPrice: {
      type: GraphQLFloat
    },
    imageUrl: {
      type: GraphQLString
    },
    priceOverPeriodOfTime: {
      type: new GraphQLList(PriceOverTimeType),
      async resolve(parent) {
        console.log(parent.id);
        const query = await Product.find({
          _id: mongoose.Types.ObjectId(parent.id)
        }).select('priceOverPeriodOfTime');
        const priceList = query[0].priceOverPeriodOfTime;
        return priceList;
      }
    },
    farmer: {
      type: FarmerType,
      resolve(parent) {
        const farmer = Farmer.findById(parent.farmerId);
        return farmer;
      }
    },
    purchasedHistory: {
      type: new GraphQLList(PurchasedItemType),
      async resolve(parent) {
        let purchasedHistory = [];
        let result = await Product.find({
          _id: mongoose.Types.ObjectId(parent.id)
        }, {
          buyers: 1,
          _id: 0
        });

        console.log(result[0].buyers);
        let buyers = result[0].buyers;
        buyers.map((buyer, index) => {
          const checkBuyerIdExisted = obj => obj.buyerId === buyer.buyerId;
          if (!purchasedHistory.some(checkBuyerIdExisted)) {
            
            purchasedHistory.push({
              buyerId: buyer.buyerId,
              productId: parent.id,
              purchasedDate: buyer.purchasedDate
            });
          }
        });
        console.log('WTF', purchasedHistory);
        // console.log(buyers);
        // User.findById(userId)

        return purchasedHistory;
      }
    }
  })
});

export default ProductType;