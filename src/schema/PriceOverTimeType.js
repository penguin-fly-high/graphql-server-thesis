import {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLFloat,
  GraphQLString
} from 'graphql';
import GraphQLDate from 'graphql-date';
import moment from 'moment';

const PriceOverTimeType = new GraphQLObjectType({
  name: "PriceOverTime",
  fields: () => ({
    price: {
      type: GraphQLFloat 
    },
    updatedDate: {
      type: GraphQLDate,
    },
    unixDate: {
      type: GraphQLString,
      resolve(parent, args) {
        return moment(parent.updatedDate).unix();
      }
    }
  })
});

export default PriceOverTimeType;