import { GraphQLID, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from "graphql";
import mongoose from "mongoose";
import Product from "../models/Product";
import User from "../models/User";
import PurchasedItemType from "./PurchasedItemType";


const UserType = new GraphQLObjectType({
  name: "User",
  fields: () => ({
    id: { type: GraphQLID },
    userId: { type: GraphQLID },
    username: { type: GraphQLString },
    token: { type: GraphQLString },
    role: { type: GraphQLString },
    age: { type: GraphQLInt },
    emailAddress: { type: GraphQLString },
    district: { type: GraphQLString },
    street: { type: GraphQLString },
    province: { type: GraphQLString },
    ward: { type: GraphQLString },
    city: { type: GraphQLString },
    purchasedItems: {
      type: new GraphQLList(PurchasedItemType),
      resolve(parent) {
        return getPurchasedItems(parent);
        // return null;

      }
    }
  })
});

async function getPurchasedItems({id}) {
  let user = await User.findById(mongoose.Types.ObjectId(id));

  if (!user) {
    throw new Error;
  }

  let productIds = [];
  user.purchasedItems.map(item => {
    productIds.push(item.productId);
  });
  let products = await Product.find({
    _id: { $in: productIds}
  });

  return products;
}

export default UserType;
