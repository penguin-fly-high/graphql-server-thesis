import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLList
} from "graphql";
import ProductType from "./ProductType";
import Product from "../models/Product";
import UserType from './UserType';

const BuyerType = new GraphQLObjectType({
  name: "Buyer",
  fields: () => ({
    id: { type: GraphQLID },
    username: { type: GraphQLString },
    age: { type: GraphQLInt },
    emailAddress: { type: GraphQLString },
    district: { type: GraphQLString },
    street: { type: GraphQLString },
    province: { type: GraphQLString },
    ward: { type: GraphQLString },
    city: { type: GraphQLString },
    purchasedItems: {
      type: new  GraphQLList(ProductType),
      resolve(parent) {
        return Product.find({ "buyers": { buyerId: parent.id } });
      }
    }
  })
});

export default BuyerType;
