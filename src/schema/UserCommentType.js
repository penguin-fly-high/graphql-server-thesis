import { GraphQLObjectType, GraphQLString, GraphQLBoolean } from 'graphql';
import GraphQLDate from 'graphql-date';

const UserCommentType = new GraphQLObjectType({
  name: 'UserComment',
  fields: () => ({
    username: { type: GraphQLString },
    content: { type: GraphQLString },
    createdDate: { type: GraphQLString },
    verifiedAsUserAlreadyPurchasedItem: { type: GraphQLBoolean }
  })
});

export default UserCommentType;