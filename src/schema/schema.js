import bcrypt from "bcryptjs";
import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString
} from "graphql";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import Category from "../models/Category";
import Farmer from "../models/Farmer";
import Product from "../models/Product";
import User from "../models/User";
import {
  createFarmerSearchFilter,
  createUserSearchFilter
} from '../utils/createFilter';
import {
  errorName
} from '../utils/graphql-error-custom/constant';
import UserType from ".//UserType";
import FarmerType from "./FarmerType";
import ProductType from "./ProductType";
import PurchasedProductInputType from "./PurchaseProductInputType";
import UploadFileType from "./UploadFileType";
import UserCommentType from "./UserCommentType";

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {

    farmer: {
      type: FarmerType,
      args: {
        id: {
          type: GraphQLID
        }
      },
      async resolve(parent, {id}) {
        let farmer = await Farmer.findById(id);
        return farmer;
      }
    },

    getUserComments: {
      type: new GraphQLList(UserCommentType),
      args: {
        productId: {
          type: GraphQLID
        }
      },
      async resolve(parent, {
        productId
      }) {
        const product = await Product.findById(productId);
        if (product) {
          let comments = [];
          await Promise.all(product.userComments.map(async (comment) => {
            let verifiedAsUserAlreadyPurchasedItem = false;
            console.log(comment.userId); // who write this comment
            const product = await Product.findOne({
              _id: mongoose.Types.ObjectId(productId)
            }, {
              buyers: 1
            });
            console.log(product);
            if (product.buyers.length > 0) {
              const verifyUserAsBuyers = obj => obj.buyerId === comment.userId;
              verifiedAsUserAlreadyPurchasedItem = await product.buyers.some(verifyUserAsBuyers);
              console.log(verifiedAsUserAlreadyPurchasedItem);
            }
            const {
              content,
              createdDate,
              username
            } = comment;
            comments.push({
              content,
              username,
              createdDate,
              verifiedAsUserAlreadyPurchasedItem
            });
          }));
          return comments;
        }
        return null;
      }
    },

    farmers: {
      type: new GraphQLList(FarmerType),
      resolve() {
        return Farmer.find({});
      }
    },

    getCategories: {
      type: new GraphQLList(GraphQLString),
      async resolve() {
        let categories = [];
        let categoryDocuments = await Category.find({}, {
          _id: 0,
          __v: 0,
        });
        categoryDocuments.map(result => {
          categories.push(result.name);
        });
        return categories;
      }
    },

    login: {
      type: UserType,
      args: {
        username: {
          type: GraphQLString
        },
        password: {
          type: GraphQLString
        }
      },
      async resolve(parent, args, {
        SECRET_KEY
      }) {
        let user = await User.findOne({
          username: args.username
        });

        if (!user) {
          throw new Error("No user with that username");
        }

        const isValid = await bcrypt.compare(args.password, user.password);

        if (!isValid) {
          throw new Error("Incorrect password");
        }
        let token = await jwt.sign({
            username: user.username,
            userId: user.id,
            role: user.role
          },
          SECRET_KEY
        );

        // Return user id, user name, token when requested with this endpoint
        let result = {
          userId: user.id,
          username: user.username,
          role: user.role,
          token: token
        };

        return result;
      }
    },

    searchFarmer: {
      type: new GraphQLList(FarmerType),
      args: {
        searchBy: {
          type: GraphQLString
        },
        searchTerm: {
          type: GraphQLString
        },
        ageFrom: {
          type: GraphQLInt
        },
        ageTo: {
          type: GraphQLInt
        },
      },
      async resolve(parent, args) {
        let query = createFarmerSearchFilter(args);
        let farmers = await Farmer.find({}).and(query);
        return farmers;
      }
    },

    searchUser: {
      type: new GraphQLList(UserType),
      args: {
        searchBy: {
          type: GraphQLString
        },
        searchTerm: {
          type: GraphQLString
        },
        ageFrom: {
          type: GraphQLInt
        },
        ageTo: {
          type: GraphQLInt
        }
      },
      async resolve(parent, args, {
        userId,
        username,
        SECRET_KEY
      }) {

        let query = createUserSearchFilter(args);
        let users = await User.find({
          _id: {
            $ne: mongoose.Types.ObjectId(userId)
          }
        }).and(query);
        return users;
      }
    },

    filterBy: {
      type: new GraphQLList(ProductType),
      args: {
        searchBy: {
          type: GraphQLString
        },
        searchTerm: {
          type: GraphQLString
        },
        classification: {
          type: GraphQLString
        },
        category: {
          type: GraphQLString
        },
        location: {
          type: GraphQLString
        },
        priceFrom: {
          type: GraphQLFloat
        },
        priceTo: {
          type: GraphQLFloat
        },
        offset: {
          type: GraphQLInt
        },
        nPerPage: {
          type: GraphQLInt
        }
      },
      async resolve(
        parent, {
          searchBy,
          searchTerm,
          classification,
          category,
          location,
          priceFrom,
          priceTo,
          offset,
          nPerPage
        }
      ) {
        let products = null;
        let query = null;

        if (searchBy === "Farmer") {
          let farmerIds = await getFarmerIds(searchTerm);

          query = createFilter({
            classification,
            category,
            location,
            priceFrom,
            priceTo,
            farmerIds
          });
        }


        if (searchBy === "Product") {
          query = createFilter({
            classification,
            category,
            location,
            priceFrom,
            priceTo,
            searchTerm
          });
        }
        
        products = await Product.find({}).and(query).skip(offset).limit(nPerPage);
        return products;
      }
    },

    buyProduct: {
      type: ProductType,
      args: {
        productId: {
          type: GraphQLString
        },
        quantity: {
          type: GraphQLInt
        },
        userId: {
          type: GraphQLString
        }
      },
      async resolve(parent, args) {
        return buyProduct(args).then(result => {
          return result;
        });
      }
    },

    users: {
      type: new GraphQLList(UserType),
      args: {},
      resolve() {
        return User.find({});
      }
    },

    product: {
      type: ProductType,
      args: {
        id: {
          type: GraphQLID
        }
      },
      resolve(parent, args) {
        return Product.findById(args.id);
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: () => ({

    writeComment: {
      type: UserCommentType,
      args: {
        productId: {
          type: GraphQLID
        },
        userId: {
          type: GraphQLID
        },
        content: {
          type: GraphQLString
        },
        username: {
          type: GraphQLString
        }
      },
      async resolve(parent, {
        userId,
        productId,
        content,
        username
      }) {
        const product = await Product.findById(productId);
        if (product) {
          const userComment = {
            userId,
            username,
            content,
            createdDate: Date.now()
          };
          console.log(userComment);
          product.userComments.push(userComment);
          await product.save();
          return userComment;
        }
        return null;
      }
    },

    createProduct: {
      type: ProductType,
      args: {
        name: {
          type: GraphQLString
        },
        classification: {
          type: GraphQLString
        },
        category: {
          type: GraphQLString
        },
        location: {
          type: GraphQLString
        },
        farmerId: {
          type: GraphQLID
        },
        quantity: {
          type: GraphQLInt
        },
        price: {
          type: GraphQLFloat
        },
      },
      resolve(rootValue, {
        name,
        classification,
        category,
        location,
        farmerId,
        quantity,
        price
      }) {
        console.log(rootValue.request.file);
        const product = new Product({
          name,
          classification,
          category,
          location,
          farmerId,
          quantity,
          recentPrice: price,
          imageUrl: rootValue.request.file.path
        });
        return product.save();
      }
    },

    registerUser: {
      type: UserType,
      args: {
        username: {
          type: GraphQLString
        },
        password: {
          type: GraphQLString
        },
        age: {
          type: GraphQLInt
        },
        emailAddress: {
          type: GraphQLString
        },
        district: {
          type: GraphQLString
        },
        street: {
          type: GraphQLString
        },
        province: {
          type: GraphQLString
        },
        ward: {
          type: GraphQLString
        },
        city: {
          type: GraphQLString
        }
      },
      async resolve(parent, args) {
        let existingUser = await User.findOne({
          username: args.username
        });

        if (existingUser) {
          // if there existed that user, then no registration allowed
          throw new Error("User already existed");
        }

        let user = new User({
          username: args.username,
          password: await bcrypt.hash(args.password, 10),
          age: args.age,
          address: {
            street: args.street,
            province: args.province,
            ward: args.ward,
            city: args.city,
            district: args.district
          },
          emailAddress: args.emailAddress
        });
        return user.save();
      }
    },

    deleteUser: {
      type: GraphQLBoolean,
      args: {
        id: {
          type: GraphQLID
        }
      },
      async resolve(parent, {
        id
      }) {
        let result = await User.findOneAndDelete({
          _id: mongoose.Types.ObjectId(id)
        });
        if (result) {
          return true;
        }
        return false;
      }
    },

    deleteProduct: {
      type: GraphQLBoolean,
      args: {
        id: { type: GraphQLID }
      },
      async resolve(parent, { id }) {
        let result = await Product.findOneAndDelete({
          _id: mongoose.Types.ObjectId(id)
        });
        
        if (result) {
          return true;
        }
        return false;
      }
    },

    deleteFarmer: {
      type: GraphQLBoolean,
      args: {
        id: { type: GraphQLID }
      },
      async resolve(parent, { id }) {
        let deletedFarmer = await Farmer.findOneAndDelete({
          _id: mongoose.Types.ObjectId(id)
        });

        if (deletedFarmer) { // farmer already deleted
          let deletedProduct = await Product.deleteMany({
            farmerId: id
          });

          // Also delete his product
          if (deletedProduct) {
            return true;
          }
        }
        return false;
      }
    },

    uploadImage: {
      type: UploadFileType,
      args: {
        name: {
          type: GraphQLString
        },
        age: {
          type: GraphQLInt
        }
      },
      async resolve(rootValule, args) {
        console.log(rootValule.request.file);
        console.log(args.age);
        console.log(args.name);
      }
    },

    // TODO
    buyProducts: {
      type: GraphQLBoolean,
      args: {
        input: {
          type: new GraphQLList(PurchasedProductInputType)
        }
      },
      async resolve(parent, {
        input
      }) {
        // return buyProductsV2(input)
        //   .then(result => {
        //     console.log('WTF result ', result);
        //     resolve(true);
        //   })
        //   .catch(err => {
        //     console.log('WTF err', err);
        //     throw new Error("Error during transaction process");
        //   });
        // try {
        //   let result = await buyProductsV2(input);
        //   return result;
        // } catch (err) {
        //   throw err;
        // }
        try {
          // return await buyProductsV2(input);
          return await buyProductsV3(input);
        } catch (err) {
          throw err;
        }
      }
    },

    editProduct: {
      type: ProductType,
      args: {
        id: {
          type: GraphQLID
        },
        name: {
          type: GraphQLString
        },
        classification: {
          type: GraphQLString
        },
        category: {
          type: GraphQLString
        },
        location: {
          type: GraphQLString
        },
        farmerId: {
          type: GraphQLID
        },
        quantity: {
          type: GraphQLInt
        },
        price: {
          type: GraphQLFloat
        }
      },
      async resolve(rootValue, args) {
        let product = await Product.findById(args.id);

        if (!product) {
          throw new Error("Product not found");
        }

        product.name = args.name ? args.name : product.name;
        product.classification = args.classification ?
          args.classification :
          product.classification;
        product.category = args.category ? args.category : product.category;
        product.quantity = args.quantity ? args.quantity : product.quantity;
        product.location = args.location ? args.location : product.location;
        product.farmerId = args.farmerId ? args.farmerId : product.farmerId;
        product.recentPrice = args.price ? args.price : product.recentPrice;
        if (rootValue.request.file) {
          product.imageUrl = rootValue.request.file.path ? rootValue.request.file.path : '';
        }
        if (args.price && product.recentPrice !== args.price) {
          product.priceOverPeriodOfTime.push({
            price: args.price,
            updatedDate: Date.now()
          });
        }
        return product.save();
      }
    },

    addProduct: {
      type: ProductType,
      args: {
        name: {
          type: GraphQLString
        },
        classification: {
          type: GraphQLString
        },
        category: {
          type: GraphQLString
        },
        location: {
          type: GraphQLString
        },
        farmerId: {
          type: GraphQLID
        },
        quantity: {
          type: GraphQLInt
        },
        price: {
          type: GraphQLFloat
        }
      },
      resolve(parent, args, {}) {
        let product = new Product({
          name: args.name,
          classification: args.classification,
          category: args.category,
          location: args.location,
          farmerId: args.farmerId,
          quantity: args.quantity,
          recentPrice: args.price
        });
        return product.save();
      }
    },

    addCategory: {
      type: GraphQLString,
      args: {
        name: {
          type: GraphQLString
        }
      },
      async resolve(parent, args) {
        let category = new Category({
          name: args.name
        });
        await category.save();
        return category.name;
      }
    },

    editFarmer: {
      type: FarmerType,
      args: {
        id: {
          type: GraphQLID
        },
        name: {
          type: GraphQLString
        },
        age: {
          type: GraphQLInt
        },
        address: {
          type: GraphQLString
        }
      },
      async resolve(rootValue, {
        name,
        age,
        address,
        id
      }) {
        const farmer = await Farmer.find({
          _id: mongoose.Types.ObjectId(id)
        });
        farmer.name = name;
        farmer.age = age;
        farmer.address = address;
        farmer.imageUrl = rootValue.request.file.path ? rootValue.request.file.path : '';
        return farmer.save();
      }
    },

    addFarmer: {
      type: FarmerType,
      args: {
        name: {
          type: GraphQLString
        },
        age: {
          type: GraphQLInt
        },
        address: {
          type: GraphQLString
        },
      },
      resolve(rootValue, {
        name,
        age,
        address
      }) {
        let farmer = new Farmer({
          name,
          age,
          address,
          imageUrl: rootValue.request.file.path ? rootValue.request.file.path : ''
        });
        return farmer.save();
      }
    }
  })
});

export default new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});

async function buyProduct({
  productId,
  userId,
  quantity
}) {
  const session = await mongoose.startSession();
  session.startTransaction();
  const userSession = await mongoose.startSession();
  userSession.startTransaction();
  let product = null;
  let user = null;
  try {
    //  Fields validation
    if (
      validateFields({
        productId,
        userId,
        quantity
      }) === false
    ) {
      throw new Error("Input fields are invalid");
    }

    const opts = {
      session,
      new: true
    };
    product = await Product.findOneAndUpdate({
        _id: mongoose.Types.ObjectId(productId)
      }, {
        $inc: {
          quantity: -quantity
        },
        $push: {
          buyers: {
            buyerId: userId,
            purchasedDate: Date.now()
          }
        }
      },
      opts
    );

    if (product.quantity < quantity && product.quantity < 0) {
      throw new Error("Insufficient quantity instock");
    }

    await session.commitTransaction();

    const userOpts = {
      userSession,
      new: true
    };
    // TODO: check existing price with requested price from client
    user = await User.findOneAndUpdate({
        _id: mongoose.Types.ObjectId(userId)
      }, {
        $set: {
          productId: productId
        },
        $push: {
          purchasedItems: {
            productId: productId,
            purchasedDate: Date.now(),
            quantity: quantity
          }
        }
      },
      userOpts
    );
    await userSession.commitTransaction();

    return new Promise((resolve, reject) => {
      if (product && user) {
        resolve(product);
      }
    });
  } catch (err) {
    await session.abortTransaction();
    await userSession.abortTransaction();
    // return new Promise((resolve, reject) => {

    // })
    throw err;
  } finally {
    session.endSession();
    userSession.endSession();
    // if (product && user) {
    //   return product;
    // } else {
    //   return null;
    // }
  }
}

function getFarmerIds(farmerName) {
  return new Promise((resolve, reject) => {
    Farmer.find({
      name: farmerName
    }, {
      name: 0,
      age: 0,
      address: 0,
      __v: 0
    }).then(result => {
      let farmerIds = result.map(obj => {
        return obj._id;
      });
      console.log(farmerIds);
      resolve(farmerIds);
    });
  });
}

function validateFields({
  productId,
  userId,
  quantity
}) {
  if (
    Number.isInteger(quantity) === false ||
    mongoose.Types.ObjectId.isValid(productId) === false ||
    mongoose.Types.ObjectId.isValid(userId) === false
  ) {
    return false;
  }
  return true;
}

function createFilter({
  classification,
  category,
  location,
  priceFrom,
  priceTo,
  farmerIds,
  searchTerm
}) {
  let query = [{}];

  if (searchTerm) {
    query.push({
      name: { $regex: searchTerm ? searchTerm : '', $options: 'i' }
    });
  }

  if (farmerIds && farmerIds.length !== 0) {
    query.push({
      farmerId: {
        $in: farmerIds
      }
    });
  }

  if (category) {
    query.push({
      category
    });
  }

  if (location) {
    query.push({
      location
    });
  }

  if (priceFrom && priceTo) {
    query.push({
      recentPrice: {
        $gte: priceFrom,
        $lte: priceTo
      }
    });
  }

  if (classification) {
    query.push({
      classification
    });
  }
  
  return query;
}

async function buyProducts(input) {
  return new Promise((resolve, reject) => {
    input.map(itemInCart => {
      buyProduct(itemInCart).then(product => {
        if (product) {
          console.log(product);
          resolve(true);
        } else {
          reject("Some of products are currently out of stock");
        }
      });
    });
  });
}

const buyProductsV3 = async (cart) => {
  let productIds = [];
  await Promise.all(cart.map(itemInCart => {
    productIds.push(itemInCart.productId);
  }));

  let session = null;
  try {
    session = await mongoose.startSession();
    session.startTransaction({
      readConcern: {
        level: 'snapshot'
      },
      writeConcern: {
        w: 'majority'
      }
    });
    await Promise.all(cart.map(async (itemInCart) => {
      const product = await Product.findById(itemInCart.productId);
      product.quantity = product.quantity - itemInCart.quantity;
      product.buyers.push({
        buyerId: itemInCart.userId,
        purchasedDate: Date.now()
      });
      await product.save({
        session
      });

      await User.findOneAndUpdate({
        _id: mongoose.Types.ObjectId(itemInCart.userId)
      }, {
        $set: {
          productId: itemInCart.productId
        },
        $push: {
          purchasedItems: {
            productId: itemInCart.productId,
            purchasedDate: Date.now(),
            quantity: itemInCart.quantity
          }
        }
      }, {
        session
      });
      console.log(product.quantity);
      if (product.quantity < 0) {
        throw new Error('Insufficient quantity in stock');
      }
    }));

    await session.commitTransaction();
    session.endSession();
    return true;
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    throw err;
  }
};

async function buyProductsV2(input) {
  return new Promise(async (resolve, reject) => {
    let sessions = [];
    let success = false;

    await input.map(async function (itemInCart) {
      try {
        const session = await mongoose.startSession();
        session.startTransaction();
        const userSession = await mongoose.startSession();
        userSession.startTransaction();
        let product = null;
        let user = null;
        const {
          productId,
          quantity,
          userId
        } = itemInCart;
        //  Fields validation
        if (validateFields(itemInCart) === false) {
          throw new Error("Input fields are invalid");
        }

        const opts = {
          session,
          new: true
        };
        product = await Product.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(productId)
          }, {
            $inc: {
              quantity: -quantity
            },
            $push: {
              buyers: {
                buyerId: userId,
                purchasedDate: Date.now()
              }
            }
          },
          opts
        );

        sessions.push(session);

        if (product.quantity < quantity && product.quantity < 0) {
          // addToErrProductList({id: product._id, name: product.name});
          throw new Error(errorName.INSUFFICIENTQUANTITY);
        }

        await session.commitTransaction();

        const userOpts = {
          userSession,
          new: true
        };
        // TODO: check existing price with requested price from client
        user = await User.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(userId)
          }, {
            $set: {
              productId: productId
            },
            $push: {
              purchasedItems: {
                productId: productId,
                purchasedDate: Date.now(),
                quantity: quantity
              }
            }
          },
          userOpts
        );
        sessions.push(userSession);
        console.log(sessions.length);
        await userSession.commitTransaction();
        success = true;
        resolve(true);
      } catch (err) {
        await sessions.map(async function (_session) {
          await _session.abortTransaction();
          _session.endSession();
        });
        sessions = [];
        reject(err);
      }
    });
  });
}