import { GraphQLObjectType, GraphQLInt, GraphQLString } from 'graphql';

const UploadFileType = new GraphQLObjectType({
    name: 'UploadFile',
    fields: () => ({
        name: { type: GraphQLString },
        age: { type: GraphQLInt },

    })
});

export default UploadFileType;