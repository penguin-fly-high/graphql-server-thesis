import { GraphQLID, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from "graphql";
import Product from "../models/Product";
import ProductType from "./ProductType";

const FarmerType = new GraphQLObjectType({
  name: "Farmer",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    address: { type: GraphQLString },
    products: {
      type: new GraphQLList(ProductType),
      resolve(parent) {
        return Product.find({ farmerId: parent.id });
      }
    }
  })
});

export default FarmerType;
