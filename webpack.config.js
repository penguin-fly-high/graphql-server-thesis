const path = require('path');
// const nodeExternals = require('webpack-node-externals');

module.exports = {
  target: 'node',
  entry: {
    app: ['@babel/polyfill','./src/app.js'],
    test: ['@babel/polyfill','./test/test.js']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-transform-async-to-generator']
          }
        },
        exclude: /node_modules/
      }
    ]
  },
  mode: "development"
}